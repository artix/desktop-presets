# desktop-presets
## Desktop presets for live media

There are 4 profile presets: desktop, gtk, qt and community.

In *desktop* are files and settings common to all ISOs (font settings, gtk-[234], kdeglobals and .face), necessary for uniform look across toolkits.

In *gtk* and *qt* are settings for the basic DEs and additionally for the applications and helpers that accompany them.

The *community* collection pulls all 3 above and adds preset configurations for several common applications and programs.

The resulting packages don't have conflicting files and can be used together to create fully uniform-looking installations.
