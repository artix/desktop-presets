[Appearance]
AntiAliasFonts=true
BoldIntense=false
ColorScheme=Linux
Font=Roboto Mono,11,-1,5,50,0,0,0,0,0,Regular

[General]
Name=Profile 1
Parent=FALLBACK/

[Interaction Options]
OpenLinksByDirectClickEnabled=true
AutoCopySelectedText=true
MiddleClickPasteMode=1
